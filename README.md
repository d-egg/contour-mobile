# Contour Mobile

Blend files to explore contour.

**Conventions**

- `CS`: I'm using this scene as a construction site to draft widgets, and other
  scenes to instantiate them.
- `S-` prefix for collections that are meant to be shared (linked) across
  scenes.  Changes there affect other scenes (bidir)
- `w-` prefix for collections meant to be instantiated as widgets (shared source
  collection)
- `o-` prefix for object-data meant to be instantiated as linked-duplicates (eg
  shared source mesh). They would have one replica in the CS scene for
  itemization.
- `__` prefix for collections that only serve conceptual grouping (on the CS)

I'm using the checkboxes in the outliner to quickly turn features on/off.  When
a second stable scene emerges, it gets extracted into a distinct one, possibly
sharing common setups by linking collections between scenes.

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img
alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work
is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons
Attribution-ShareAlike 4.0 International License</a>.
